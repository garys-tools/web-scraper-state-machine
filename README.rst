=========================
Web Scraper State Machine
=========================

Introduction
============

This is a terrible name for a library.

How it works
============

When looking around for a simple FSM package in python I did not find
anything that was light, simple and mature.  So I decided to create
my own.  One of the things I like about using a FSM is the opportunity
for having a nice diagram to illustrate it's operations.  From there,
I wondered if I could create a FSM that uses PlantUML as the basis
for its declaration combined with some simple conventions for the
implementation.

For state diagrams, see here:  https://plantuml.com/state-diagram

To create a diagram, see here:  https://www.plantuml.com/plantuml/uml

State Machine Workflow
======================

First, create a PlantUML State Diagram and assign this to a StateMachine class.

All states go through the following process:

* on_enter_state - This is an event raised when a state is entered, it should
  execute any output actions and terminate with minial blocking
* on_input_state - When the FSM receives an input it is passed to the active state,
  the state will then call start_transition()
* on_exit_state - This is called after an input event to indicate a state exit


Notes
=====

Browser integration notes
* Map events in the browser to inputs into the FSM
* Focus on async


TODO
====

The reason this is here is so that the state machine can be maintained
independently of my other projects... and also improve upon it as I don't
think that web scraping is anything I will stop soon.
