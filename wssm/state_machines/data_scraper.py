# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
This is a general purpose records scraper implemented as an FSM.

Essentially the state machine does not need to change as this is a
common workflow.  Instead, the DataInterface class needs to be specified
and provided to a child of the state machine.

"""
import abc
import asyncio
import logging
from typing import List, Any, Optional

from uml_fsm.state_machine import StateMachine


logger = logging.getLogger(__name__)


class DataInterface:
    """
    The scope of the interface class is to act as an API for managing
    the interface to the data.  Typically, this will be a browser, but
    it could also be a request object.

    NOTE:  Inherit CdtInterface.  This is just an interface declaration
    that is useful for testing.

    This class is responsible for the collection of data and creating
    a common interface for that collection.

    This class represents the interface needed for the scraper
    state machine to perform actions and receive events from a
    data system.

    The interface will also identify the state of the browser as this
    information is needed for the associated state machine to also
    change state.
    """

    entry_url = "https://www.google.com"

    def __init__(self, notify_fx, username: str = "", password: str = ""):
        self.password = password
        self.username = username
        self.notify_state_change: callable = notify_fx

    async def go_to_login(self):
        await self.navigate_to_url(self.entry_url)

    # ------------- Implemented by CDTInterface and others -----------------

    # noinspection PyMethodMayBeStatic
    async def open(self):
        logger.debug("open")

    @abc.abstractmethod
    async def is_browser_idle(self) -> bool:
        """
        Implemented by child classes to indicate the browser is ready
        and idle
        """
        return True

    # noinspection PyMethodMayBeStatic
    async def navigate_to_url(self, url: str):
        logger.debug("navigate to url %s", url)

    # noinspection PyMethodMayBeStatic
    async def reset_context_data(self):
        """
        A hook method for resetting any context stored by the data
        interface information prior to a context switch
        """
        pass

    # noinspection PyMethodMayBeStatic
    async def close(self):
        logger.debug("close")

    # ------------- Implemented by child classes -----------------

    # noinspection PyMethodMayBeStatic
    async def execute_login(self):
        """
        Perform the actions to get the browser into a login state
        """
        logger.debug("Login Action Called")

    # noinspection PyMethodMayBeStatic
    async def switch_context(self, context_data: Any):
        """
        Perform the actions to prepare the browser for collecting data
        """
        logger.debug("Switching Context %s", context_data)

    # noinspection PyMethodMayBeStatic
    async def load_records(self, current_context: Any) -> List[Any]:
        """
        Perform the actions to collect and return data
        """
        logger.debug("Load Records in context %s", current_context)
        return []


class DataScraper(StateMachine):
    # noinspection SpellCheckingInspection,GrazieInspection
    """
    :ivar data:  The data captured by this machine
    :ivar data_contexts:  A list of different contexts to process
    """

    # noinspection SpellCheckingInspection
    PLANTUML = """
    @startuml
    Init : Browser and homepage loading
    LoggingIn : Automated login
    LoggedIn : Checking context
    SwitchingContext : Triggering data
    LoadingRecords : Parsing data
    ProcessingRecords: Storing or taking action on the data
    
    
    [*] --> Init
    Init --> LoggingIn : login_ready
    LoggingIn --> LoggedIn : logged_in
    LoggedIn --> SwitchingContext : switch_context
    LoggedIn --> Done : context_done
    SwitchingContext --> LoadingRecords : context_ready
    LoadingRecords --> ProcessingRecords : records_loaded
    ProcessingRecords --> LoggedIn : records_processed
    Done --> [*]
    @enduml
    """

    UML_DIAGRAM = "https://www.plantuml.com/plantuml/uml/RPB1QiCm38RlVWgV1ts17aQtxbBOeQoxZH649PAmn2goIhUtdw8icqSAWTZwln_f5zuL1T7BE8H3xZLkuwFmjP14o1Wx7kaCBSM10VlSXcTkM_iVie4F5-KHbD1KAxf6wDA-e_HXN4oSbJurdAwzfiug-xbWpAlqnij48IYOtLkyK6B1Oi0HfFogH-54fVm1stXITZTBLF1kaBJd7EtJZcPV26ztxt6pkOzJFC_ejpg8Zzy80NvL0HUAS87is5GvdRWHh7YfMQB_mvyuao4_MeDsNUt6mTKs5iioPYsxOxKXCybyQWR32SCQgKDM_7a6pJ8Dx9IjCUmeexsKRm00"  # noqa: e501

    data: List[Any]
    data_contexts: List[Any]
    data_interface: DataInterface

    def __init__(self, data_contexts: List[Any] = None, username="", password=""):
        self.password = password
        self.username = username
        self.data = []
        self.data_interface = self.init_data_interface()
        self.data_contexts: List[Any] = data_contexts if data_contexts else []
        self.current_data_context: Optional[Any] = None
        super(DataScraper, self).__init__()

    def init_data_interface(self) -> DataInterface:
        return DataInterface(self.notify_input, self.username, self.password)

    async def on_enter_init(self):
        await self.data_interface.open()
        await self.data_interface.go_to_login()

    async def test_login_ready(self):
        return await self.data_interface.is_browser_idle()

    async def on_enter_logging_in(self):
        try:
            await self.data_interface.execute_login()
        except Exception as e:
            logger.error("Login failed, trying again: %s", e, exc_info=True)
            await self.data_interface.execute_login()

    async def test_logged_in(self):
        return await self.data_interface.is_browser_idle()

    async def on_enter_logged_in(self):
        self.current_data_context = None
        await self.data_interface.reset_context_data()

    async def test_switch_context(self):
        return bool(self.data_contexts)

    async def test_context_done(self):
        return not self.data_contexts

    async def on_enter_switching_context(self):
        self.current_data_context = self.data_contexts[0]
        # Don't mutate original list, just take a splice
        self.data_contexts = self.data_contexts[1:]
        await self.data_interface.switch_context(self.current_data_context)

    # noinspection PyMethodMayBeStatic
    async def test_context_ready(self):
        return await self.data_interface.is_browser_idle()

    async def on_enter_loading_records(self):
        records = await self.data_interface.load_records(self.current_data_context)
        self.data += records

    # noinspection PyMethodMayBeStatic
    async def test_records_loaded(self):
        return await self.data_interface.is_browser_idle()

    # noinspection PyMethodMayBeStatic
    async def on_enter_processing_records(self):
        logger.info("Processing records is not implemented")

    # noinspection PyMethodMayBeStatic
    async def test_records_processed(self):
        return True

    # noinspection PyMethodMayBeStatic
    async def on_enter_done(self):
        logger.debug("Waiting before closing browser")
        await self.data_interface.close()
        await asyncio.sleep(2)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    sm = DataScraper(["Context1", "Context2"])
    asyncio.run(sm.run_to_completion())
