# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
This is an updated scraper implementation.  This focuses on general
scraping tools.

Due to the asynchronous nature of web scraping (and a move towards
chrome devtools to access XHR details etc...), we're creating a simple
state machine representation of web scraping.

Note that this state machine is designed to execute asynchronously to
allow external events to modify / force the state of the machine.
"""
import pathlib

DATA_ROOT = pathlib.Path(__file__).parent.parent / "data"
HEADLESS = False
