# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import asyncio
import io
import logging
import os
import pathlib
import time
from typing import TypedDict, Dict, List, TypeVar, Tuple, NamedTuple

import PIL

from wssm import DATA_ROOT
from wssm.scraper_engines.image import (
    ocr_image_prep,
    compare_all_images_ssim,
    ImageDimensions,
    ImageCoords,
)

logger = logging.getLogger(__name__)


class WindowRef(TypedDict):
    guid: str
    title: str


ElementReference = TypeVar("ElementReference")


class TabInfo(NamedTuple):
    title: str
    url: str


class SelectByText(NamedTuple):
    css_selector: str
    contains_text: str


class WebsiteMixin:
    """
    This class declares a common interface that is needed to be provided
    by any website driver.  It describes common routines.

    This has evolved from the original simple_scraper code.
    """

    async def set_input_element_value(
        self, css_selector, val, retry=3, frame_css: str = None
    ):
        """
        This implementation is intended to do it's best to set the value of
        an input element.

        If there are multiple elements, it only sets the first element it
        comes across.

        :raises RuntimeError:  If the value can not be set.
        """
        raise NotImplementedError

    async def list_tabs(self) -> List[TabInfo]:
        raise NotImplementedError

    async def go_to_tab(self, tab: TabInfo):
        raise NotImplementedError

    async def roll_back_tab(self) -> str:
        """
        Returns to the previous tab from the last "go_to_tab" call.  It
        will also close the current tab
        """
        raise NotImplementedError

    async def get_screenshot_as_png(self) -> bytes:
        raise NotImplementedError

    async def save_screenshot(self, output_name: str):
        raise NotImplementedError

    async def get_page_html(self) -> str:
        raise NotImplementedError

    async def list_elements_by_selector(
        self, css_selector: str, web_component_root: str = None, frame_css: str = None
    ) -> List[ElementReference]:
        raise NotImplementedError

    async def element_is_accessible(self, e: ElementReference, frame_css: str = None):
        """
        Typically an is_displayed and is_enabled test
        """
        raise NotImplementedError

    async def wait_for_selector(self, css_selector, frame_css: str = None):
        """
        :raises RuntimeError:  When no element exists
        """
        raise NotImplementedError

    async def list_accessible_elements_by_selector(
        self,
        css_selector,
        timeout=15,
        web_component_root=None,
        frame_css: str = None,
    ) -> List[ElementReference]:
        """
        WAS check_for_element

        web_component_root is needed for web components in a shadowRoot.
        """
        start = time.time()
        e = []
        while not e and (start + timeout) >= time.time():
            # For asyncio, we can't use filter expressions, so good ol' for
            # noinspection PyBroadException
            try:
                for x in await self.list_elements_by_selector(
                    css_selector,
                    web_component_root=web_component_root,
                    frame_css=frame_css,
                ):
                    if await self.element_is_accessible(x, frame_css=frame_css):
                        e.append(x)
            except Exception:
                await asyncio.sleep(1)
        return e

    async def list_accessible_child_elements_by_selector(
        self,
        element_reference: ElementReference,
        css_selector: str,
        timeout=15,
        frame_css: str = None,
    ) -> List[ElementReference]:
        """
        WAS check_for_element
        """
        start = time.time()
        e = []
        while not e and (start + timeout) >= time.time():
            # For asyncio, we can't use filter expressions, so good ol' for
            for x in await self.list_child_elements_by_selector(
                element_reference, css_selector
            ):
                if await self.element_is_accessible(x, frame_css=frame_css):
                    e.append(x)
        return e

    async def list_child_elements_by_selector(
        self, element_reference: ElementReference, css_selector: str
    ):
        """
        :raises RuntimeError:  When no element exists
        """
        raise NotImplementedError

    async def click_on_element_reference(self, element: ElementReference):
        raise NotImplementedError

    async def click_on_element_by_selector(
        self, css_selector: str, web_component_root: str = None, frame_css: str = None
    ):
        raise NotImplementedError

    async def click_on_element_by_selector_with_aria_label(
        self,
        css_selector: SelectByText,
        label_includes: str = None,
        web_component_root: str = None,
    ):
        raise NotImplementedError

    async def click_on_element_by_selector_with_text(
        self, css_selector: SelectByText, web_component_root: str = None
    ):
        raise NotImplementedError

    async def get_element_text_by_selector(
        self, css_selector: str, frame_css: str = None
    ) -> str:
        """
        :raises RuntimeError:  If an element is not found
        """
        raise NotImplementedError

    async def get_element_text_by_reference(
        self, ref: ElementReference, frame_css: str = None
    ) -> str:
        """
        :raises RuntimeError:  If an element is not found
        """
        raise NotImplementedError

    async def get_element_attribute_by_reference(
        self, ref: ElementReference, attribute_name: str, frame_css: str = None
    ) -> str:
        """
        :raises RuntimeError:  If an element is not found
        """
        raise NotImplementedError

    async def get_element_ref_position(
        self, element: ElementReference, frame_css: str = None
    ) -> Tuple[ImageCoords, ImageDimensions]:
        raise NotImplementedError
        # For Selenium
        # element_pos = current_element.location
        # element_size = current_element.size

    async def get_elements_by_images(
        self, css_selector: str, images_folder: str, full_set: set
    ) -> Dict[str, ElementReference]:
        """
        WAS get_elements_by_images

        Matches elements to a series of png files given in the image_folder.
        The resulting text will be the file name

        :param full_set: A set containing all expected images required for this
        operation.  This is for assertion.

        :param images_folder: THe name of the sub-folder in the data parent folder

        :param css_selector: string for selection of elements to snapshot

        :return: A dictionary lookup of the matching image name and a reference to
            the matching element
        """
        logger.info("Preparing Image Match")
        image_logging = True

        # Load button elements first, this should cause wait for loading to occur:
        elements = await self.list_accessible_elements_by_selector(css_selector)

        full_screen_data = await self.get_screenshot_as_png()
        # noinspection PyUnresolvedReferences
        full_screen_image = PIL.Image.open(io.BytesIO(full_screen_data))
        sample_images = {}

        # Load Sample image Files and store a ref dimension for exact image sizing
        ref_dim = None
        for image_file_name in (DATA_ROOT / images_folder).glob("*.png"):
            # noinspection PyUnresolvedReferences
            image_data = PIL.Image.open(image_file_name)
            if ref_dim is None:
                ref_dim = image_data.size
            image_file_name = os.path.splitext(os.path.basename(image_file_name))[0]
            sample_images[image_file_name] = ocr_image_prep(image_data, ref_dim)

        logger.debug("Image matches found: %s", sample_images)

        image_match = {}

        assert full_set.issubset(
            set(sample_images.keys())
        ), "Images must exist for all set provided"

        image_logging_root = pathlib.Path("/tmp/raik_cf/img_log")
        if image_logging and not image_logging_root.exists():
            image_logging_root.mkdir(parents=True)
        if image_logging:
            logger.debug("Exporting image logs to: %s", image_logging_root)
            full_screen_image.save(str(image_logging_root / "full_screen.png"))

        # For all elements matching the CSS Selector, compare them to the
        # sample images.  If there is strong a match, pops the sample image from the set,
        # so it can not be reused.
        element_count = 0
        for current_element in elements:
            element_count += 1
            element_pos, element_size = await self.get_element_ref_position(
                current_element
            )
            # This is strange, positions / sizes seem to be half actual.
            # Could be because of DPI Scaling

            trim = 4
            box = (
                element_pos["x"] + trim,
                element_pos["y"] + trim,
                element_pos["x"] + element_size["width"] - 2 * trim,
                element_pos["y"] + element_size["height"] - 2 * trim,
            )

            logger.debug(
                f"Element {element_count}, Pos {element_pos}  "
                f"Size: {element_size} box: {box}"
            )

            element_image = full_screen_image.crop(box)
            element_image = ocr_image_prep(element_image, ref_dim)
            if image_logging:
                element_image.save(
                    str(image_logging_root / ("element_%i.png" % element_count))
                )

            score_to_name = compare_all_images_ssim(
                element_image,
                sample_images,
                debug=True,
                output_temp=True,
                image_logging_root=image_logging_root,
            )
            logger.debug("Image match element %i:  %s", element_count, score_to_name)

            # Find the smallest RMS and assign the image
            best_score = min(score_to_name.keys())
            element_match = score_to_name[best_score]
            logger.debug(
                "Image match element %i:  Best Score %0.3f (%s)",
                element_count,
                best_score,
                element_match,
            )

            # Ensure unambiguous match
            assert element_match not in image_match, (
                "Test %s has already been matched" % element_match
            )
            image_match[element_match] = current_element

        return image_match
