# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
This module implements a basic state machine for the loading and
interaction with a Chrome browser.
"""
import asyncio
import threading
from subprocess import check_output, CalledProcessError
from typing import Optional, List, Tuple
import logging
import pyppeteer
from pyppeteer.browser import Browser
from pyppeteer.element_handle import ElementHandle
from pyppeteer.network_manager import Response as ChromeResponse
from pyppeteer.network_manager import Request as ChromeRequest


# from pyppeteer.launcher import DEFAULT_ARGS
from pyppeteer.page import Page

# Patch chrome args for better container support if required (not recommended)
# DEFAULT_ARGS.append("--no-sandbox")
from wssm.scraper_engines.image import ImageCoords, ImageDimensions
from wssm.scraper_engines.website_api import (
    WebsiteMixin,
    ElementReference,
    TabInfo,
    SelectByText,
)
from wssm.state_machines.data_scraper import DataScraper, DataInterface

logger = logging.getLogger(__name__)

BROWSER_EXECUTABLE = ["chromium-browser", "chromium"]


def get_browser_path() -> Optional[str]:
    output = None
    for b in BROWSER_EXECUTABLE:
        try:
            output = check_output(["which", b]).decode().strip()
        except CalledProcessError:
            # No command found
            pass
    return output


class CdtInterface(DataInterface, WebsiteMixin):
    """
    Chrome Devtools API interface

    # To bypass detection, read the following article:
    # https://intoli.com/blog/not-possible-to-block-chrome-headless/

    # This might even work for headless mode!
    """

    click_options = {"delay": 0.2}
    user_agent_string = (
        "Mozilla/5.0 (X11; Fedora; Linux x86_64) "
        "AppleWebKit/537.36 (KHTML, like Gecko) "
        "Chrome/99.0.4844.74 Safari/537.36"
    )

    def __init__(self, *args):
        super().__init__(*args)
        self._browser: Optional[Browser] = None
        self._page: Optional[Page] = None
        self.current_tab: Optional[TabInfo] = None
        self._context_sent_requests: List[ChromeRequest] = []
        self._all_sent_requests: List[ChromeRequest] = []
        self._context_requests_count = 0
        self._all_requests_count = 0
        self._context_done_requests_count = 0
        self._all_done_requests_count = 0
        self._context_responses_received: List[ChromeResponse] = []
        self._all_responses_received: List[ChromeResponse] = []
        self._page_stack: List[Tuple[Page, TabInfo]] = []
        self._first_load_complete = False
        # DANGER:  This is a threading lock on purpose because either
        # the page events return in a new thread, or the page events
        # will return within the main event loop.  We won't
        # know because the page events are not asyncio callbacks.
        self._counters_lock = threading.Lock()

    async def _connect_to_page(self, page: Page):
        """Connects events to a page"""
        # This decorates the "on_page_response" function
        # which is seen in the pyee documentation (which is a
        # dependency of the PyPPeteer library).
        page.on("response")(self._on_response_received)
        page.on("request")(self._on_request_sent)
        # noinspection SpellCheckingInspection
        page.on("requestfinished")(self._on_request_finished)
        # noinspection SpellCheckingInspection
        page.on("requestfailed")(self._on_request_failed)
        page.on("load")(self._on_load)

    def _disconnect_page(self, page: Page):
        page.remove_listener("response", self._on_response_received)
        page.remove_listener("request", self._on_request_sent)
        # noinspection SpellCheckingInspection
        page.remove_listener("requestfinished", self._on_request_finished)
        # noinspection SpellCheckingInspection
        page.remove_listener("requestfailed", self._on_request_failed)
        page.remove_listener("load", self._on_load)

    def _on_request_sent(self, request: ChromeRequest):
        # logger.debug("Request to: %s", request.url)
        with self._counters_lock:
            self._context_requests_count += 1
            self._context_sent_requests.append(request)
            self.notify_state_change()

    # noinspection PyUnusedLocal
    def _on_request_failed(self, request: ChromeRequest):
        # logger.debug("Request failed: %s", request.url)
        with self._counters_lock:
            self._context_done_requests_count += 1
            self.notify_state_change()

    # noinspection PyUnusedLocal
    def _on_request_finished(self, request: ChromeRequest):
        # logger.debug("Request finished: %s", request.url)
        with self._counters_lock:
            self._context_done_requests_count += 1
            self.notify_state_change()

    def _on_response_received(self, response: ChromeResponse):
        # logger.debug("Response from: %s", response.url)
        with self._counters_lock:
            self._context_responses_received.append(response)
            self.notify_state_change()

    # noinspection PyMethodMayBeStatic
    def _on_load(self, *args):
        """
        This might get called in a separate thread
        """
        logger.debug("Load complete %s", args)
        # This trigger can only be raised once
        if not self._first_load_complete:
            self._first_load_complete = True
            self.notify_state_change()

    async def hide_automation(self):
        """
        See class docstring.  Only a couple of defenses have been enabled,
        there are still more here:  https://intoli.com/blog/not-possible-to-block-chrome-headless/
        NAB source code obfuscation seems to convert all values to an encoded ASCII representation
        that I have not been able to decode.  I am curious as to when in the loading process it
        checks for the webdriver.

        More useful links:
        https://stackoverflow.com/questions/57131736/how-to-unset-navigator-webdriver-in-headless-chrome-using-selenium

        """
        logger.info("Executing hide_automation")
        await self._page.setUserAgent(self.user_agent_string)
        await self._page.evaluateOnNewDocument(
            """() => {
                Object.defineProperty(navigator, "webdriver", {get: () => false,});
            }""",
        )
        # noinspection SpellCheckingInspection
        await self._page.evaluateOnNewDocument(
            """() => {
                window.navigator.chrome = {
                  app: {
                    isInstalled: false,
                  },
                  webstore: {
                    onInstallStageChanged: {},
                    onDownloadProgress: {},
                  },
                  runtime: {
                    PlatformOs: {
                      MAC: 'mac',
                      WIN: 'win',
                      ANDROID: 'android',
                      CROS: 'cros',
                      LINUX: 'linux',
                      OPENBSD: 'openbsd',
                    },
                    PlatformArch: {
                      ARM: 'arm',
                      X86_32: 'x86-32',
                      X86_64: 'x86-64',
                    },
                    PlatformNaclArch: {
                      ARM: 'arm',
                      X86_32: 'x86-32',
                      X86_64: 'x86-64',
                    },
                    RequestUpdateCheckStatus: {
                      THROTTLED: 'throttled',
                      NO_UPDATE: 'no_update',
                      UPDATE_AVAILABLE: 'update_available',
                    },
                    OnInstalledReason: {
                      INSTALL: 'install',
                      UPDATE: 'update',
                      CHROME_UPDATE: 'chrome_update',
                      SHARED_MODULE_UPDATE: 'shared_module_update',
                    },
                    OnRestartRequiredReason: {
                      APP_UPDATE: 'app_update',
                      OS_UPDATE: 'os_update',
                      PERIODIC: 'periodic',
                    },
                  },
                }
            }""",
        )

    # ------------- WebsiteMixin -----------------

    async def _get_page(self, frame_css=None):
        if frame_css:
            frame_handle = await self.list_elements_by_selector(frame_css)
            frame = await frame_handle[0].contentFrame()
            return frame
        else:
            return self._page

    async def set_input_element_value(self, css_selector, val, retry=3, frame_css=None):
        p = await self._get_page(frame_css)
        await p.focus(css_selector)
        await p.querySelectorEval(
            css_selector,
            """(el) => {
                el.value = "";
            }""",
        )
        await p.type(css_selector, val)

    async def get_screenshot_as_png(self) -> bytes:
        return await self._page.screenshot({"type": "png"})

    async def list_elements_by_selector(
        self, css_selector: str, web_component_root=None, frame_css=None
    ) -> List[ElementHandle]:
        p = await self._get_page(frame_css)
        if web_component_root:
            web_root = await p.querySelector(web_component_root)
            # cmd = f"(el) => (el.shadowRoot.querySelectorAll('{css_selector}'))"
            p = await web_root.getProperty("shadowRoot")
            shadow_root = p.asElement()
            v = await shadow_root.querySelectorAll(css_selector)
            return v
        else:
            return await p.querySelectorAll(css_selector)

    async def list_child_elements_by_selector(
        self, element_reference: ElementHandle, css_selector: str
    ):
        """
        :raises RuntimeError:  When no element exists
        """
        return await element_reference.querySelectorAll(css_selector)

    async def get_element_text_by_selector(
        self, css_selector: str, frame_css=None
    ) -> str:
        p = await self._get_page(frame_css)
        return await p.querySelectorEval(
            css_selector,
            """(el) => {
                return el.textContent
            }""",
        )

    async def get_element_text_by_reference(
        self, ref: ElementHandle, frame_css=None
    ) -> str:
        p = await self._get_page(frame_css)
        return await p.evaluate(
            """(el) => {
                return el.textContent
            }""",
            ref,
        )

    async def get_element_attribute_by_reference(
        self, ref: ElementHandle, attribute_name: str, frame_css=None
    ) -> str:
        p = await self._get_page(frame_css)
        return await p.evaluate(
            """(el, attr_name) => {
                return el.getAttribute(attr_name)
            }""",
            ref,
            attribute_name,
        )

    async def element_is_accessible(self, e: ElementHandle, frame_css=None):
        p = await self._get_page(frame_css)
        return await p.evaluate(
            (
                "(el) => {\n"
                "return "
                "window.getComputedStyle(el).getPropertyValue('display') !== 'none' "
                "&& el.offsetHeight "
                "&& !el.hasAttribute('disabled')\n"
                "&& !el.classList.contains('disabled')\n"
                "}"
            ),
            e,
        )

    async def wait_for_selector(self, css_selector, frame_css=None):
        p = await self._get_page(frame_css)
        await p.waitForSelector(css_selector)

    async def click_on_element_reference(self, element: ElementHandle):
        await element.click(options=self.click_options)

    async def click_on_element_by_selector(
        self, css_selector: str, web_component_root=None, frame_css=None
    ):
        buttons = await self.list_accessible_elements_by_selector(
            css_selector,
            timeout=5,
            web_component_root=web_component_root,
            frame_css=frame_css,
        )
        first_button = buttons[0]
        await first_button.click()

    async def click_on_element_by_selector_with_aria_label(
        self,
        css_selector: str,
        label_includes: str = None,
        web_component_root: str = None,
    ):
        buttons: List[ElementHandle] = await self.list_accessible_elements_by_selector(
            css_selector, timeout=5, web_component_root=web_component_root
        )
        buttons_aria_labels = [await b.getProperty("aria-label") for b in buttons]
        for button, text in zip(buttons, buttons_aria_labels):
            if label_includes in text:
                await button.click()
                return
        raise ValueError(f"Button not found for {css_selector}")

    async def click_on_element_by_selector_with_text(
        self, css_selector: SelectByText, web_component_root: str = None
    ):
        buttons: List[ElementHandle] = await self.list_accessible_elements_by_selector(
            css_selector.css_selector, timeout=5, web_component_root=web_component_root
        )
        buttons_text = [await self.get_element_text_by_reference(b) for b in buttons]
        for button, text in zip(buttons, buttons_text):
            if css_selector.contains_text in text:
                await button.click()
                return
        raise ValueError(f"Button not found for {css_selector}")

    async def get_element_ref_position(
        self, element: ElementReference, frame_css=None
    ) -> Tuple[ImageCoords, ImageDimensions]:
        p = await self._get_page(frame_css)
        d = await p.evaluate(
            """(el) => {
                    i = el.getBoundingClientRect();
                    return {x: i.x, y: i.y, width: i.width, height: i.height};
            }
            """,
            element,
        )
        return (
            ImageCoords(x=int(d["x"]), y=int(d["y"])),
            ImageDimensions(width=int(d["width"]), height=int(d["height"])),
        )

    async def save_screenshot(self, output_name: str):
        pass

    async def get_page_html(self) -> str:
        pass

    async def list_tabs(self) -> List[TabInfo]:
        pages = await self._browser.pages()
        page_titles = [await t.title() for t in pages]
        page_urls = [t.url for t in pages]
        pages = []
        for i, title in enumerate(page_titles):
            url = page_urls[i]
            if url != "about:blank":
                pages.append(TabInfo(title, url))
        return pages

    async def go_to_tab(self, tab: TabInfo):
        pages = await self._browser.pages()
        page_titles = [await t.title() for t in pages]
        page_urls = [t.url for t in pages]
        pages_map = {}
        for i, title in enumerate(page_titles):
            url = page_urls[i]
            if url != "about:blank":
                pages_map[TabInfo(title, url)] = pages[i]
        new_page = pages_map[tab]
        old_page = self._page
        old_tab = self.current_tab
        self._page_stack.append((old_page, old_tab))
        await new_page.bringToFront()
        self._page = new_page
        self.current_tab = tab
        await self.hide_automation()
        await self._connect_to_page(new_page)

    async def roll_back_tab(self) -> str:
        """
        Returns to the previous tab from the last "go_to_tab" call.  It
        will also close the current tab
        """
        if not self._page_stack:
            raise RuntimeError("Rolling back a page but there is no page stack")
        old_page, old_tab = self._page_stack.pop()
        current_page = self._page
        self._page = old_page
        self.current_tab = old_tab
        self._disconnect_page(current_page)
        await current_page.close()
        return old_page.url

    async def wait_for_idle_browser(self):
        while not await self.is_browser_idle():
            await asyncio.sleep(1)

    async def refresh_browser(self):
        """
        A simple page reload
        """
        await self.reset_context_data()
        await self._page.reload()
        await self.wait_for_idle_browser()

    # ------------- DataInterface -----------------

    async def open(self):
        """
        This is called to begin a browser session
        """
        logger.debug("Starting Browser")
        vp = {"width": 1600, "height": 1024}

        from wssm import HEADLESS

        if HEADLESS:
            self._browser = await pyppeteer.launch(
                {
                    "executablePath": get_browser_path(),
                    "headless": True,
                    "defaultViewport": vp,
                    "pipe": True,
                }
            )
        else:
            self._browser = await pyppeteer.launch(
                {
                    "executablePath": get_browser_path(),
                    "headless": False,
                    "defaultViewport": vp,
                    "pipe": True,
                    "args": [f"--window-size={vp['width']},{vp['height']}"],
                }
            )
        logger.debug("Browser started")
        self._page = await self._browser.newPage()
        await self.hide_automation()
        await self._connect_to_page(self._page)
        self.current_tab = TabInfo(await self._page.title(), self._page.url)
        logger.debug("Page created")

    async def is_browser_idle(self) -> bool:
        """
        This is an interesting one as some request errors can occur (
        no server response maybe?) that aren't tracked.  Taking a more
        statistical approach until there is time to correct this
        """
        return self._first_load_complete and (
            int(self._context_requests_count * 0.95)
            <= self._context_done_requests_count
        )

    async def navigate_to_url(self, url: str):
        await self._page.goto(url)

    async def reset_context_data(self):
        """
        A hook method for resetting any context information prior to
        a switch
        """
        with self._counters_lock:
            self._all_sent_requests += self._context_sent_requests
            self._all_requests_count += self._context_requests_count
            self._all_responses_received += self._context_responses_received
            self._all_done_requests_count += self._context_done_requests_count

            self._context_sent_requests = []
            self._context_requests_count = 0
            self._context_done_requests_count = 0
            self._context_responses_received = []

    async def close(self):
        await self._browser.close()


class Website(DataScraper):
    data_interface: CdtInterface

    def init_data_interface(self) -> CdtInterface:
        return CdtInterface(self.notify_input, self.username, self.password)


def manual_test_simple_run():
    """
    A simple manual test run
    """
    sm = Website(["CDTContext1", "CDTContext1"])
    asyncio.run(sm.run_to_completion(), debug=True)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger("websockets").setLevel(logging.INFO)
    logging.getLogger("pyppeteer").setLevel(logging.INFO)

    manual_test_simple_run()
    x = 1
