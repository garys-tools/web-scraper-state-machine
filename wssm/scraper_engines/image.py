# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import os
from typing import TypedDict

import PIL.Image

# noinspection PyProtectedMember
from skimage.io._plugins.pil_plugin import pil_to_ndarray
from skimage.metrics import structural_similarity
from skimage.registration import phase_cross_correlation

image_scale = 1
crop_offset_left = 0.03
crop_offset_top = 0.05
crop_factor_width = 0.4
crop_factor_height = 0.4
resample_method = PIL.Image.NEAREST


class ImageCoords(TypedDict):
    x: int
    y: int


class ImageDimensions(TypedDict):
    width: int
    height: int


def ocr_image_prep(img: PIL.Image, ref_size) -> PIL.Image:
    """
    The image comparison seems to work better when resizing / trimming the image for
    processing.  This applies a standard pre-processing on the image.
    """
    if img.size != ref_size:
        img = img.resize(ref_size, resample=resample_method,)
    img = img.resize(
        (img.size[0] * image_scale, img.size[1] * image_scale),
        resample=resample_method,
    )
    img_width, img_height = img.size
    crop_width = img_width * crop_factor_width
    crop_left = img_width * crop_offset_left + (img_width - crop_width) / 2
    crop_height = img_height * crop_factor_height
    crop_upper = img_height * crop_offset_top + (img_height - crop_height) / 2
    return img.crop(
        (crop_left, crop_upper, crop_left + crop_width, crop_upper + crop_height,)
    )


def compare_image_ssim(img_a: PIL.Image, img_b: PIL.Image):
    img_a_num = pil_to_ndarray(img_a)
    img_b_num = pil_to_ndarray(img_b)

    shift, error, diff_phase = phase_cross_correlation(img_a_num, img_b_num)

    y, x, z = shift
    x = round(x)
    y = round(y)

    # img_a crop to support realignment
    w, h = img_a.size
    crop_width = w - abs(x)
    crop_height = h - abs(y)
    crop_x = abs(max(x, 0))
    crop_y = abs(max(y, 0))
    img_a = img_a.crop((crop_x, crop_y, crop_x + crop_width, crop_y + crop_height))
    img_a_num = pil_to_ndarray(img_a)

    # img_b
    crop_x = abs(min(x, 0))
    crop_y = abs(min(y, 0))
    img_b = img_b.crop((crop_x, crop_y, crop_x + crop_width, crop_y + crop_height))
    img_b_num = pil_to_ndarray(img_b)

    s = structural_similarity(img_a_num, img_b_num, multichannel=True)
    s = 1 - s
    # s = compare_mse(img_a_num, img_b_num)
    return s


def compare_all_images_ssim(
    img: PIL.Image,
    match_set: dict,
    output_temp=False,
    debug=None,
    image_logging_root=None,
):
    """
    Using SSIM from
    https://www.pyimagesearch.com/2014/09/15/python-compare-two-images/
    +1 = the best match from the source function, however, this is reversed
    as the calling function is expecting a min result.  This method will return
    0 to 2 with 0 being the closest match.

    :param output_temp: Output sample images to temp with a rating in the file name.
    WARNING:  Deletes temp.
    :param img: a PIL Image
    :param match_set: A dictionary of pil images to compare against.
    :param debug: Add debug output
    :param image_logging_root: Export images for debug
    :return: Dictionary of [Score: Name]
    """
    from wssm import DATA_ROOT

    if output_temp:
        for f in (DATA_ROOT / "img_tmp").glob("*.*"):
            os.remove(f)
        img.save(DATA_ROOT / "img_tmp" / "element.png")

    sample_rms = {}
    score_to_name = {}
    for file_name, sample_image in match_set.items():
        if debug is not None:
            sample_image.save(
                str(image_logging_root / f"element_{debug}-{file_name}.png")
            )
        # calculate rms
        s = compare_image_ssim(img, sample_image)
        sample_rms[file_name] = s
        assert s not in score_to_name
        score_to_name[s] = file_name
        if output_temp:
            sample_image.save(DATA_ROOT / "img_tmp" / f"{s:0.4f}.png")

    return score_to_name
